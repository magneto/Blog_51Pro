# Blog_51Pro

#### 介绍
开源的博客系统 
前后分离
SpringBoot
vue

Blog_Admin
前台是thymeleaf，
博客后台，用jpa做持久层。

Blog_Admin_Mybatis
前台是thymeleaf，
博客后台，用mybatis做持久层

Blog_view_Thymeleaf
前台的页面项目，内容不全，直接合并到相应的后台里了。

Blog_view_vue/blog_vue

使用vue做页面展示

![博客预览](https://images.gitee.com/uploads/images/2020/0730/223759_aeb38d0a_336355.png "QQ截图20200730223733.png")


博客访问网址
http://www.51pro.top/





