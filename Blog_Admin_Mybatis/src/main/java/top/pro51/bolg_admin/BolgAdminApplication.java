package top.pro51.bolg_admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BolgAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(BolgAdminApplication.class, args);
    }

}
