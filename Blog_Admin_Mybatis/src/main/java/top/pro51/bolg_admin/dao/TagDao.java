package top.pro51.bolg_admin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import top.pro51.bolg_admin.domain.Tag;

import java.util.List;

@Mapper
@Repository
public interface TagDao extends BaseMapper<Tag> {
    List<Tag> getBlogTag();
}
