package top.pro51.bolg_admin.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import top.pro51.bolg_admin.domain.User;

/**
 * @Description user的dao层
 * @Author WangWenpeng
 * @Date 0:58 2020/7/28
 * @Param
 */
@Mapper
@Repository
public interface UserDao extends BaseMapper<User> {

    /**
     * @Description 用户名登录验证
     * @Author WangWenpeng
     * @Date 9:15 2020/7/28
     * @Param [username, password]
     */
    User findByUsernameAndPassword(String username, String password);
}
