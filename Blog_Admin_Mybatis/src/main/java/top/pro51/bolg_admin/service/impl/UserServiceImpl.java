package top.pro51.bolg_admin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.pro51.bolg_admin.dao.UserDao;
import top.pro51.bolg_admin.domain.User;
import top.pro51.bolg_admin.service.UserService;
import top.pro51.bolg_admin.util.MD5Utils;


@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User checkUser(String username, String password) {
        User user = userDao.findByUsernameAndPassword(username, MD5Utils.code(password));
        return user;
    }
}
