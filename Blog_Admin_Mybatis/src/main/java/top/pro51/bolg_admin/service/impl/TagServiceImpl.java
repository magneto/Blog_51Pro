package top.pro51.bolg_admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.pro51.bolg_admin.dao.TagDao;
import top.pro51.bolg_admin.domain.Tag;
import top.pro51.bolg_admin.service.TagService;

import java.util.ArrayList;
import java.util.List;

@Service("tagService")
public class TagServiceImpl implements TagService {

    @Autowired
    TagDao tagDao;

    @Override
    public int saveTag(Tag tag) {
        return tagDao.insert(tag);
    }

    @Override
    public Tag getTag(Long id) {
        return tagDao.selectById(id);
    }

    @Override
    public Tag getTagByName(String name) {
        QueryWrapper<Tag> wapper = new QueryWrapper<Tag>();
        wapper.eq("name", name);
        return tagDao.selectOne(wapper);
    }

    @Override
    public List<Tag> getAllTag() {
        return tagDao.selectList(null);
    }

    @Override
    public List<Tag> getBlogTag() {
        return tagDao.getBlogTag();
    }

    @Override
    public List<Tag> getTagByString(String text) {    //从tagIds字符串中获取id，根据id获取tag集合
        List<Tag> tags = new ArrayList<>();
        List<Long> longs = convertToList(text);
        for (Long long1 : longs) {
            tags.add(tagDao.selectById(long1));
        }
        return tags;
    }

    private List<Long> convertToList(String ids) {  //把前端的tagIds字符串转换为list集合
        List<Long> list = new ArrayList<>();
        if (!"".equals(ids) && ids != null) {
            String[] idarray = ids.split(",");
            for (int i = 0; i < idarray.length; i++) {
                list.add(new Long(idarray[i]));
            }
        }
        return list;
    }

    @Override
    public int updateTag(Tag tag) {
        return tagDao.updateById(tag);
    }

    @Override
    public int deleteTag(Long id) {
        return tagDao.deleteById(id);
    }


}
