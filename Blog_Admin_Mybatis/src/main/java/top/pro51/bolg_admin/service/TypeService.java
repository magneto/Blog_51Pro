package top.pro51.bolg_admin.service;


import top.pro51.bolg_admin.domain.Type;

import java.util.List;

public interface TypeService {

    /**
     * @Description 新增保存
     * @Author WangWenpeng
     * @Date 9:12 2020/7/28
     * @Param [type]
     */
    int saveType(Type type);

    /**
     * @Description 用id查询一个
     * @Author WangWenpeng
     * @Date 9:12 2020/7/28
     * @Param [id]
     */
    Type getType(Long id);

    /**
     * @Description 用名称查询一个
     * @Author WangWenpeng
     * @Date 9:12 2020/7/28
     * @Param [name]
     */
    Type getTypeByName(String name);

    /**
     * @Description 修改分类
     * @Author WangWenpeng
     * @Date 9:13 2020/7/28
     * @Param [id, type]
     */
    int updateType(Type type);

    /**
     * @Description 删除分类
     * @Author WangWenpeng
     * @Date 9:13 2020/7/28
     * @Param [id]
     */
    int deleteType(Long id);

    /**
     * @Description 所有分类
     * @Author WangWenpeng
     * @Date 20:59 2020/7/29
     * @Param []
     */
    List<Type> getAllType();

    /**
     * @Description 首页右侧展示type对应的博客数量
     * @Author WangWenpeng
     * @Date 21:00 2020/7/29
     * @Param []
     */
    List<Type> getBlogType();


}
