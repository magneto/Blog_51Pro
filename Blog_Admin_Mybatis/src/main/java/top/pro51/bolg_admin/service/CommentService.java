package top.pro51.bolg_admin.service;


import top.pro51.bolg_admin.domain.Comment;

import java.util.List;

public interface CommentService {

    List<Comment> getCommentByBlogId(Long blogId);

    int saveComment(Comment comment);
}
