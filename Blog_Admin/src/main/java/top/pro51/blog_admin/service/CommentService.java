package top.pro51.blog_admin.service;


import top.pro51.blog_admin.po.Comment;

import java.util.List;


public interface CommentService {

    /**
     * @Description 评论列表
     * @Author WangWenpeng
     * @Date 16:44 2020/7/28
     * @Param [blogId]
     */
    List<Comment> listCommentByBlogId(Long blogId);

    /**
     * @Description 提交评论
     * @Author WangWenpeng
     * @Date 16:44 2020/7/28
     * @Param [comment]
     */
    Comment saveComment(Comment comment);
}
