package top.pro51.blog_admin.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import top.pro51.blog_admin.po.Type;

import java.util.List;

public interface TypeService {

    /**
     * @Description 新增保存
     * @Author WangWenpeng
     * @Date 9:12 2020/7/28
     * @Param [type]
     */
    Type saveType(Type type);

    /**
     * @Description 用id查询一个
     * @Author WangWenpeng
     * @Date 9:12 2020/7/28
     * @Param [id]
     */
    Type getType(Long id);

    /**
     * @Description 用名称查询一个
     * @Author WangWenpeng
     * @Date 9:12 2020/7/28
     * @Param [name]
     */
    Type getTypeByName(String name);

    /**
     * @Description 分类列表
     * @Author WangWenpeng
     * @Date 9:13 2020/7/28
     * @Param [pageable]
     */
    Page<Type> listType(Pageable pageable);

    /**
     * @Description 分类列表
     * @Author WangWenpeng
     * @Date 9:13 2020/7/28
     * @Param []
     */
    List<Type> listType();

    /**
     * @Description
     * @Author WangWenpeng
     * @Date 9:13 2020/7/28
     * @Param [size]
     */
    List<Type> listTypeTop(Integer size);

    /**
     * @Description 修改分类
     * @Author WangWenpeng
     * @Date 9:13 2020/7/28
     * @Param [id, type]
     */
    Type updateType(Long id, Type type);

    /**
     * @Description 删除分类
     * @Author WangWenpeng
     * @Date 9:13 2020/7/28
     * @Param [id]
     */
    void deleteType(Long id);
}
