package top.pro51.blog_admin.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import top.pro51.blog_admin.po.Tag;

import java.util.List;


public interface TagService {

    /**
     * @Description 新增标签
     * @Author WangWenpeng
     * @Date 11:35 2020/7/28
     * @Param [type]
     */
    Tag saveTag(Tag type);

    /**
     * @Description id查询
     * @Author WangWenpeng
     * @Date 11:35 2020/7/28
     * @Param [id]
     */
    Tag getTag(Long id);

    /**
     * @Description name查询
     * @Author WangWenpeng
     * @Date 11:35 2020/7/28
     * @Param [name]
     */
    Tag getTagByName(String name);

    /**
     * @Description 分页查询
     * @Author WangWenpeng
     * @Date 11:35 2020/7/28
     * @Param [pageable]
     */
    Page<Tag> listTag(Pageable pageable);

    /**
     * @Description 列表
     * @Author WangWenpeng
     * @Date 11:35 2020/7/28
     * @Param []
     */
    List<Tag> listTag();

    /**
     * @Description 列表
     * @Author WangWenpeng
     * @Date 11:35 2020/7/28
     * @Param [size]
     */
    List<Tag> listTagTop(Integer size);

    /**
     * @Description id查询
     * @Author WangWenpeng
     * @Date 11:35 2020/7/28
     * @Param [ids]
     */
    List<Tag> listTag(String ids);

    /**
     * @Description 修改标签
     * @Author WangWenpeng
     * @Date 11:36 2020/7/28
     * @Param [id, type]
     */
    Tag updateTag(Long id, Tag type);

    /**
     * @Description 删除标签
     * @Author WangWenpeng
     * @Date 11:36 2020/7/28
     * @Param [id]
     */
    void deleteTag(Long id);
}
