package top.pro51.blog_admin.service;


import top.pro51.blog_admin.po.User;

public interface UserService {
    /**
     * @Description 登录的用户名密码检查
     * @Author WangWenpeng
     * @Date 0:56 2020/7/28
     * @Param [username, password]
     */
    User checkUser(String username, String password);
}
