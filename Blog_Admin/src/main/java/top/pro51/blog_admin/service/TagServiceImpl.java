package top.pro51.blog_admin.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.pro51.blog_admin.NotFoundException;
import top.pro51.blog_admin.dao.TagRepository;
import top.pro51.blog_admin.po.Tag;

import java.util.ArrayList;
import java.util.List;


@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepository tagRepository;

    /**
     * @Description 新增标签
     * @Author WangWenpeng
     * @Date 11:36 2020/7/28
     * @Param [tag]
     */
    @Transactional
    @Override
    public Tag saveTag(Tag tag) {
        return tagRepository.save(tag);
    }

    /**
     * @Description id查询
     * @Author WangWenpeng
     * @Date 11:37 2020/7/28
     * @Param [id]
     */
    @Transactional
    @Override
    public Tag getTag(Long id) {
        return tagRepository.getOne(id);
    }

    /**
     * @Description name查询
     * @Author WangWenpeng
     * @Date 11:37 2020/7/28
     * @Param [name]
     */
    @Override
    public Tag getTagByName(String name) {
        return tagRepository.findByName(name);
    }

    /**
     * @Description 分页查询
     * @Author WangWenpeng
     * @Date 11:37 2020/7/28
     * @Param [pageable]
     */
    @Transactional
    @Override
    public Page<Tag> listTag(Pageable pageable) {
        return tagRepository.findAll(pageable);
    }

    /**
     * @Description 列表
     * @Author WangWenpeng
     * @Date 11:37 2020/7/28
     * @Param []
     */
    @Override
    public List<Tag> listTag() {
        return tagRepository.findAll();
    }

    @Override
    public List<Tag> listTagTop(Integer size) {
        Pageable pageable = PageRequest.of(0, size, Sort.by(Sort.Direction.DESC, "blogs.size"));
        return tagRepository.findTop(pageable);
    }

    /**
     * @Description id查询
     * @Author WangWenpeng
     * @Date 11:40 2020/7/28
     * @Param [ids]
     */
    @Override
    public List<Tag> listTag(String ids) { //1,2,3
        return tagRepository.findAllById(convertToList(ids));
    }

    private List<Long> convertToList(String ids) {
        List<Long> list = new ArrayList<>();
        if (!"".equals(ids) && ids != null) {
            String[] idarray = ids.split(",");
            for (int i = 0; i < idarray.length; i++) {
                list.add(new Long(idarray[i]));
            }
        }
        return list;
    }

    /**
     * @Description 修改标签
     * @Author WangWenpeng
     * @Date 11:41 2020/7/28
     * @Param [id, tag]
     */
    @Transactional
    @Override
    public Tag updateTag(Long id, Tag tag) {
        Tag t = tagRepository.getOne(id);
        if (t == null) {
            throw new NotFoundException("不存在该标签");
        }
        BeanUtils.copyProperties(tag, t);
        return tagRepository.save(t);
    }

    /**
     * @Description 删除标签
     * @Author WangWenpeng
     * @Date 11:40 2020/7/28
     * @Param [id]
     */
    @Transactional
    @Override
    public void deleteTag(Long id) {
        tagRepository.deleteById(id);
    }
}
