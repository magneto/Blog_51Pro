package top.pro51.blog_admin.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import top.pro51.blog_admin.po.Blog;
import top.pro51.blog_admin.po.BlogQuery;

import java.util.List;
import java.util.Map;


public interface BlogService {

    /**
     * @Description 主键查询
     * @Author WangWenpeng
     * @Date 12:03 2020/7/28
     * @Param [id]
     */
    Blog getBlog(Long id);

    /**
     * @Description 获取内容转换为html
     * @Author WangWenpeng
     * @Date 16:27 2020/7/28
     * @Param [id]
     */
    Blog getAndConvert(Long id);

    /**
     * @Description 参数查询分页
     * @Author WangWenpeng
     * @Date 12:03 2020/7/28
     * @Param [pageable, blog]
     */
    Page<Blog> listBlog(Pageable pageable, BlogQuery blog);

    Page<Blog> listBlog(Pageable pageable);

    /**
     * @Description 查tag相关
     * @Author WangWenpeng
     * @Date 18:32 2020/7/28
     * @Param [tagId, pageable]
     */
    Page<Blog> listBlog(Long tagId, Pageable pageable);

    Page<Blog> listBlog(String query, Pageable pageable);

    List<Blog> listRecommendBlogTop(Integer size);

    /**
     * @Description 按时间年份整理
     * @Author WangWenpeng
     * @Date 18:46 2020/7/28
     * @Param []
     */
    Map<String, List<Blog>> archiveBlog();

    Long countBlog();

    /**
     * @Description 新增
     * @Author WangWenpeng
     * @Date 12:04 2020/7/28
     * @Param [blog]
     */
    Blog saveBlog(Blog blog);

    /**
     * @Description 博客修改
     * @Author WangWenpeng
     * @Date 12:04 2020/7/28
     * @Param [id, blog]
     */
    Blog updateBlog(Long id, Blog blog);

    /**
     * @Description 博客删除
     * @Author WangWenpeng
     * @Date 12:04 2020/7/28
     * @Param [id]
     */
    void deleteBlog(Long id);
}
