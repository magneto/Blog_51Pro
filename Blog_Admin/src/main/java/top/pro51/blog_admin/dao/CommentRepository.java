package top.pro51.blog_admin.dao;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import top.pro51.blog_admin.po.Comment;

import java.util.List;


public interface CommentRepository extends JpaRepository<Comment, Long> {

    /**
     * @Description 评论信息的回复信息 单表父节点查询子节点
     * @Author WangWenpeng
     * @Date 16:45 2020/7/28
     * @Param [blogId, sort]
     */
    List<Comment> findByBlogIdAndParentCommentNull(Long blogId, Sort sort);

}
