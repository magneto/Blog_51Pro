package top.pro51.blog_admin.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.transaction.annotation.Transactional;
import top.pro51.blog_admin.po.Blog;

import java.util.List;


public interface BlogRepository extends JpaRepository<Blog, Long>, JpaSpecificationExecutor<Blog> {

    /**
     * @Description 查找最新页面
     * @Author WangWenpeng
     * @Date 13:52 2020/7/28
     * @Param [pageable]
     */
    @Query("select b from Blog b where b.recommend = true")
    List<Blog> findTop(Pageable pageable);

    /**
     * @Description 带搜索条件 标题或者描述
     * @Author WangWenpeng
     * @Date 13:53 2020/7/28
     * @Param [query, pageable]
     */
    @Query("select b from Blog b where b.title like ?1 or b.description like ?1")
    Page<Blog> findByQuery(String query, Pageable pageable);

    /**
     * @Description 修改博客
     * @Author WangWenpeng
     * @Date 13:53 2020/7/28
     * @Param [id]
     */
    @Transactional
    @Modifying
    @Query("update Blog b set b.views = b.views+1 where b.id = ?1")
    int updateViews(Long id);

    /**
     * @Description 年统计
     * @Author WangWenpeng
     * @Date 13:53 2020/7/28
     * @Param []
     */
    @Query("select function('date_format',b.updateTime,'%Y') as year from Blog b group by function('date_format',b.updateTime,'%Y') order by year desc ")
    List<String> findGroupYear();

    /**
     * @Description 年统计
     * @Author WangWenpeng
     * @Date 13:53 2020/7/28
     * @Param [year]
     */
    @Query("select b from Blog b where function('date_format',b.updateTime,'%Y') = ?1")
    List<Blog> findByYear(String year);
}
