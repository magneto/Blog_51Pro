package top.pro51.blog_admin.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import top.pro51.blog_admin.po.User;

/**
 * @Description user的dao层
 * @Author WangWenpeng
 * @Date 0:58 2020/7/28
 * @Param
 */
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * @Description 用户名登录验证
     * @Author WangWenpeng
     * @Date 9:15 2020/7/28
     * @Param [username, password]
     */
    User findByUsernameAndPassword(String username, String password);
}
