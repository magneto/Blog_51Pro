package top.pro51.blog_admin.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import top.pro51.blog_admin.po.Type;

import java.util.List;

/**
 * @Description 分类信息dao层
 * @Author WangWenpeng
 * @Date 9:15 2020/7/28
 * @Param
 */
public interface TypeRepository extends JpaRepository<Type, Long> {

    /**
     * @Description 通过名称查询
     * @Author WangWenpeng
     * @Date 9:15 2020/7/28
     * @Param [name]
     */
    Type findByName(String name);

    /**
     * @Description 分页
     * @Author WangWenpeng
     * @Date 9:15 2020/7/28
     * @Param [pageable]
     */
    @Query("select t from Type t")
    List<Type> findTop(Pageable pageable);
}
