package top.pro51.blog_admin.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AboutShowController {

    /**
     * @Description 跳转关于我页面
     * @Author WangWenpeng
     * @Date 18:57 2020/7/28
     * @Param []
     */
    @GetMapping("/about")
    public String about() {
        return "about";
    }
}
