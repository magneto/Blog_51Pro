package top.pro51.blog_admin.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import top.pro51.blog_admin.service.BlogService;


@Controller
public class ArchiveShowController {

    @Autowired
    private BlogService blogService;

    /**
     * @Description 时间线统计
     * @Author WangWenpeng
     * @Date 18:45 2020/7/28
     * @Param [model]
     */
    @GetMapping("/archives")
    public String archives(Model model) {
        model.addAttribute("archiveMap", blogService.archiveBlog());
        model.addAttribute("blogCount", blogService.countBlog());
        return "archives";
    }
}
